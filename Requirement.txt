asgiref==3.2.3
Django==3.0.2
pip==19.3.1
psycopg2==2.8.4
pytz==2019.3
setuptools==44.0.0
sqlparse==0.3.0
wheel==0.33.6