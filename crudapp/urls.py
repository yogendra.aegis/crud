from django.urls import path
from . import views
from django.conf.urls import url

#Here use URL if you append any value, for simple page use path. 
urlpatterns = [
    path('index/', views.index, name='index'),
    url(r'^index/(?P<id>\d+)/$', views.delete, name='delete'),
    url(r'^update/(?P<id>\d+)/$', views.update, name='update'),
]
