# Generated by Django 3.0.2 on 2020-01-02 13:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='chatMessage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('message', models.CharField(max_length=25)),
            ],
            options={
                'db_table': 'user',
            },
        ),
    ]
