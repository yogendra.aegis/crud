from django.db import models

# Create your models here.


class chatMessage(models.Model):
    
    id = models.AutoField(primary_key=True)
    message = models.CharField(max_length=25)
    
    class Meta:
        db_table = "crudData"
