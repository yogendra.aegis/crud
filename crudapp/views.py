from django.shortcuts import render, redirect
from crudapp.models import chatMessage
from crudapp.forms import chatMessageForm
from django.shortcuts import get_object_or_404
# Create your views here.

def index(request):
    if request.method == "POST":
        form = chatMessageForm(request.POST)
        chatMessage1 = chatMessage.objects.all()

        if form.is_valid():
            form.save()
            chatMessage1 = chatMessage.objects.all()
            return redirect('/crudapp/index/')
        else:
            return render(request, 'index.html', {"chatMessage1": chatMessage1})
    else:
        chatMessage1 = chatMessage.objects.all()
        return render(request, 'index.html', {"chatMessage1": chatMessage1})


def delete(request, id):
    instance = chatMessage.objects.get(id=id)
    instance.delete()
    return redirect('/crudapp/index/')

def update(request, id):
    user_update = get_object_or_404(chatMessage, pk=id)

    if request.method == "POST":
        user_update.message = request.POST['message']
        user_update.save()
        return redirect('/crudapp/index/')
    else:
        return render(request, 'profile.html', {'user_update': user_update})

    
