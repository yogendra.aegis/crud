
from django import forms
from .models import chatMessage


class chatMessageForm(forms.ModelForm):
    class Meta:
        model = chatMessage
        fields = "__all__"
